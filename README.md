# Xbiome DevOps Engineering System Challenge

## Repository Contents

<details><summary>deploystack.py</summary>
    This script reads a cloudformation template and applies it to the given AWS region
</details>

<details><summary>templates.json</summary>
    AWS CloudFormation template for creating Ec2 Instance, IAM User, EFS, Mount Target, CloudWatch Alarm
</details>

<details><summary>requirements.txt</summary>
    Dependencies required for running the python script
</details>

Steps to be followed for running this project:

 - [ ] Download latest version of Python and install it in the local machine and then install pip
 (https://www.python.org/downloads/)
 
 
 - [ ] Clone this repository to the local machine and navigate to the directory where it is cloned
 
 - [ ] Install the dependencies for the python script using below command

 ```sh
 pip install -r requirements.txt
 ```
 
 - [ ] Install Boto3(AWS SDK for Python) to use AWS services using below command
 

 ```sh
 pip install boto3
 ```

 - [ ] Create IAM User with Full Access to S3, EC2, CloudWatch, CloudFormation, EFS and enable Programmatic Access for the user and save the Secret Key and Access Key

 - [ ] Use below command to Configure Boto3 (AWS SDK for Python) with IAM Credentials
 
 ```sh
 aws configure
 ```
 and Provide Secret Key, Access Key, Default Region and Output Format.



 ## Services or Features needed as prerequisite from AWS Account 

| Service/Feature | Purpose |
| ------ | ------ |
| KeyPair | To SSH into Ec2 Instance |
| SecurityGroup | To Use with EC2 Instance and Mount Target |
| SNS Topic | For Sending SNS Notification |
| SNS Subscription | For configuring Notification channel |
| Subnet ID | Used by EC2 and EFS |


## Parameters required for Running the script

| ParameterName | Value | Sample Value |
| ------ | ------ | ------ |
| InstanceType | Type of Instance need to be provisioned | t3a.micro |
| ImageId | AMI ID of the Image | ami-0747bdcabd34c712a |
| BucketName | S3 Bucket Name | hpc-test |
| SNSArn | ARN of the **SNS Topic** | arn:aws:sns:us-east-1:096180274696:Xbiome |
| KeyName | EC2 KeyPair Name | Xbiome |
| subnetId | ID of the Subnet in any Availability Zone within VPC | subnet-b0e09eef |
| securityGroupId | Ec2 Security Group ID to use with EC2 and EFS | sg-02b86d4db6984b49a |

Use below command like below along with the parameter values to run the script

```sh
python3 deploystack.py -t template.json -n xbiome -r us-east-1 -p "InstanceType=t3a.micro&ImageId=ami-0747bdcabd34c712a&BucketName=hpc-test&SNSArn=arn:aws:sns:us-east-1:096180274696:Xbiome&KeyName=Xbiome&subnetId=subnet-b0e09eef&securityGroupId=sg-02b86d4db6984b49a"
```
> _Note: Replace Parameters Values in above command_

Once the command is executed Python script produces CF Stack Plan like below:

```sh
[ INFO ] Template Description: AWS CloudFormation template for creating Ec2 Instance, IAM User, EFS, Mount Target, CloudWatch Alarm


[ INFO ] The following resources will be created:


------------------------------------------------------
| Count  |    Resource Type  
------------------------------------------------------
| 1      |    AWS::EC2::Instance
------------------------------------------------------
| 1      |    AWS::IAM::User 
------------------------------------------------------
| 1      |    AWS::EFS::FileSystem
------------------------------------------------------
| 1      |    AWS::EFS::MountTarget
------------------------------------------------------
| 1      |    AWS::CloudWatch::Alarm
------------------------------------------------------
```

Script prompts for User Input. Enter **yes** then the script starts applying CF Template.

Once the stack Creation is completed. Script returns below output.

```sh
[ OK ]  Cloudformation Stack xbiome State: CREATE_COMPLETE


[ INFO ] Stack xbiome completed
```

## Possible Errors:

1.  botocore.errorfactory.AlreadyExistsException: An error occurred (AlreadyExistsException) when calling the CreateStack operation: Stack [xbiome] already exists

    Solution : Delete the Stack manually in AWS Console (Cloud Formation) and retry the script.




